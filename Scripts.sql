CREATE TABLE person
(
    id         BIGINT       NOT NULL,
    name       VARCHAR(255) NULL,
    email      VARCHAR(255) NULL,
    password   VARCHAR(255) NULL,
    created    datetime     NULL,
    modified   datetime     NULL,
    last_login datetime     NULL,
    active     BIT(1)       NULL,
    CONSTRAINT pk_person PRIMARY KEY (id)
);

ALTER TABLE person
    ADD CONSTRAINT uc_e9d568832bde9ae4a06f37c5e UNIQUE (email);


	
	CREATE TABLE phone
(
    id          INT          NOT NULL,
    person_id   BIGINT       NULL,
    number      VARCHAR(255) NULL,
    city_code   VARCHAR(255) NULL,
    contry_code VARCHAR(255) NULL,
    CONSTRAINT pk_phone PRIMARY KEY (id)
);

ALTER TABLE phone
    ADD CONSTRAINT FK_PHONE_ON_PERSON FOREIGN KEY (person_id) REFERENCES person (id);